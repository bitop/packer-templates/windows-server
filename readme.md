# Packer with HCL and vsphere-iso

This repo gives a few examples using the latest packer (v1.5.0+) and HCL. HCL enables a lot of neat new abilities.

## Usage

debian has its own source and builder block in the debian.pkr.hcl file. Each specific OS version (IE debian 9.13 or debian 10.7) has its own variable file that contains the unique variables for the os. This means that you can build debian 9.13 from the same builder block as debian 10.7.

- `debian.pkr.hcl` is the main packer configuration file.
- `variables.pkr.hcl` initializes all of the variables used buy packer.
- `vsphere.auto.pkrvars.hcl` has all of the values to connect to the vsphere environment
- `9.13.pkrvars.hcl` contains the values to build debian 9.13.1 LTS
- `10.7.pkrvars.hcl` contains the values to build debian 10.7

Examples:

debian 9.13: `packer build --only vsphere-iso.debian --var-file=9.13.pkrvars.hcl .`

debian 10.7: `packer build --only vsphere-iso.debian --var-file=10.7.pkrvars.hcl .`

Note the trailing `.` at the end of the command. That is telling packer to build everything in the current directory. This is key for any auto.pkrvars.hcl to be automatically populated.

## Directories 

`http` - Stores files to Kickstart OS's
`_common` - Shared scripts
`scripts` - Scripts used to bootstrap OS's

## Keys Files

### `debian.pkr.hcl`

This is where the sources and builds are defined. Every build is triggered from this file.

### `vsphere.auto.pkr.hcl`

These are variables that are loaded automatically (anything with a `.auto.pkrvars.hcl` will load automatically in the same directory) and are the same for every build. This is where the vsphere variables are populated.

### `variables.auto.pkr.hcl`

This is where the hcl variables are declared. You could potentially put this at the top of your `debian.pkr.hcl` file, but it's good practice to keep your variables files separate.

## Individual OS variable files

These files are specifically provided during the packer command to tell packer which build to process.

Examples:

debian 9.13: `packer build --only vsphere-iso.debian --var-file=9.13.pkrvars.hcl .`

debian 10.7: `packer build --only vsphere-iso.debian --var-file=10.7.pkrvars.hcl .`

Note the trailing period in that command. That is telling packer to build everything in the directory, which is required to get the auto.pkrvars.hcl to work correctly. Note in that command the `--only` flag. That tells packer to only build the `vsphere-iso.debian` source.